/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da_thuc;

/**
 *
 * @author le cong hieu
 */
public class Expression 
{
    private StackofChar infix;
    private StackofChar posfix;
    private int value;
    
    public Expression(){
        infix = null;
        posfix = null;
        value = 0;
    }
    public Expression(StackofChar stack)
    {
        this.infix = stack;
        posfix = null;
        value = 0;
    }
    
    public void setInfix(StackofChar infix)
    {
        this.infix = infix;
    }      
    public StackofChar getInfix()
    {
        return infix;
    }
    public void setPosfix()
    {
        posfix = toPosfix(infix);
    }
    public StackofChar getPosfix()
    {
        return posfix;
    }
    public void setValue()
    {
        value = toValue(posfix);
    }
    public int getValue()
    {
        return value;
    }
    
    private static boolean isOperator(char c){
        return (c=='+') || (c=='-') || (c=='*') || (c=='/');
    }
    private static int priorityOfOperator(char c){
        if( (c=='+')||(c=='-') )
            return 1;
        if( (c=='*')||(c=='/') )
            return 2;
        return 0;
    }
    
    private static StackofChar toPosfix(StackofChar st)
    {
        StackofChar operatorStack = new StackofChar(10000);
        String result = "";

        while(!st.isEmpty())
        {
             if(st.peak()==')')
            {
                while(!operatorStack.isEmpty() && operatorStack.peak()!='(')
                {
                    result += operatorStack.pop();
                }
                if(!operatorStack.isEmpty())
                {
                    operatorStack.pop();
                }
                st.pop();
            }
            else if(st.peak() == '(')
            {
                operatorStack.push(st.pop());
            }
            else if(Expression.isOperator(st.peak()))
            {
                while(!operatorStack.isEmpty() && Expression.priorityOfOperator(st.peak())<= Expression.priorityOfOperator(operatorStack.peak())){
                    result += operatorStack.pop();
                }
                operatorStack.push(st.pop());
            
            }
            else
            {
                result += st.pop();
            }
        }
        
        while(!operatorStack.isEmpty()){
            result += operatorStack.pop();
        }
        
        StackofChar RS = new StackofChar(result.length());
        int i = result.length()-1;
        while(i>= 0)
        {
            RS.push(result.charAt(i));
            i--;
        }
        return RS;
    }
    
    private static int toValue(StackofChar st)
    {
        if(st.isEmpty()) return 0;
        
        int rs = 0;
        StackofInt stackofint = new StackofInt(st.getSize());
        
        while(!st.isEmpty())
        {
            char X = st.pop();
            if(Expression.isOperator(X))
            {
                int k1 = stackofint.pop();
                int k2 = stackofint.pop();
                if(X == '+')
                {
                    stackofint.push(k2+k1);
                }
                else if(X == '-')
                {
                    stackofint.push(k2-k1);
                }
                else if(X == '*')
                {
                    stackofint.push(k2*k1);
                }
                else if(X == '/')
                {
                    stackofint.push(k2/k1);
                }
            }
            else
            {
                stackofint.push(Integer.parseInt(""+X));
            }
        }
        rs = stackofint.pop();
        return rs;
    }
}