/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da_thuc;
import java.util.Scanner;
/**
 *
 * @author le cong hieu
 */

public class Process_Expression 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner imput = new Scanner(System.in);
        String exp_str;
        exp_str = "6*(5+4)+3*(2-1)";
        
        StackofChar exp_sta = new StackofChar(exp_str.length());
        for(int i = exp_str.length()-1;i >=0;i--)
        {
            exp_sta.push(exp_str.charAt(i));
        }
        
        Expression exp = new Expression();
        System.out.println("Hien thi: ");
        
        exp.setInfix(exp_sta);
        System.out.print("Trung to: "); exp.getInfix().print();
        
        exp.setPosfix();
        System.out.print("Hau to: "); exp.getPosfix().print();
        
        exp.setValue();
        System.out.println("Ket qua: " + exp.getValue());
        
    }
    
}
