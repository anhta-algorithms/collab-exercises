/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da_thuc;

/**
 *
 * @author le cong hieu
 */
class StackofChar {
    char[] elements = null;
    int size;
    
    public StackofChar()
    {
        size = 0;
        //elements = new char[16];
    }
    public StackofChar(int capacity)
    {
        size = 0;
        elements = new char[capacity+1];
    }
    
    public boolean isEmpty()
    {
        if(size == 0) return true;
        else return false;
    }
    public boolean isFull()
    {
        if(size == elements.length) return true;
        else return false;
    }
    public char peak()
    {
        return elements[size-1];
    }
    public void push(char ch)
    {
        elements[size] = ch;
        size++;
    }
    public char pop()
    {
        char getvalue = elements[size-1];
        size--;
        return getvalue;
    }
    public int getSize()
    {
        return size;
    }
    
    public void print()
    {
        
        if(size == 0) System.out.print("Empty");
            
        int i = size-1;
        while(i>=0){
            System.out.print(elements[i] + " ");
            i--;
            if(i == -1) break;
        }
        System.out.println("");
    }
}
