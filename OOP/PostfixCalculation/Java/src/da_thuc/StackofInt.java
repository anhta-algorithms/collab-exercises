/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package da_thuc;

/**
 *
 * @author tuan
 */
public class StackofInt 
{
    int[] stackofint;
    int size;
    
    StackofInt()
    {
        size = 0;
    }
    StackofInt(int capacity)
    {
        size = 0;
        stackofint = new int[capacity];
    }
    
    public boolean isEmpty()
    {
        if(size == 0) return true;
        return false;
    }
    public boolean isFull()
    {
        if(size == stackofint.length) return true;
        return false;
    }
    public int pop()
    {
        int rs = stackofint[size-1];
        size--;
        return rs;
    }
    public void push(int value)
    {
        size++;
        stackofint[size-1] = value;
    }
    public int peak()
    {
        return stackofint[size-1];
    }
    public int getSize()
    {
        return size;
    }
}
