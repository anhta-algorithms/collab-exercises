# encode: utf8

from collections import deque


class Stack():

    """Docstring for Stack. """

    def __init__(self, init_stack):
        """TODO: to be defined1. """
        self._stack = deque(init_stack)

    def push(self, val):
        self._stack.appendleft(val)

    def pop(self):
        return self._stack.popleft()

    def get_stack(self):
        return self._stack


class Queue():

    """Docstring for Queue. """

    def __init__(self, init_queue):
        """TODO: to be defined1. """
        self._queue = deque(init_queue)

    def push(self, val):
        self._queue.append(val)

    def pop(self):
        return self._queue.popleft()

    def get_queue(self):
        return self._queue


def main():
    my_stack = Stack([])
    my_queue = Queue([])

    my_stack.push(10)
    my_stack.push(15)
    my_stack.push(20)
    print(my_stack.pop())
    print(my_stack.get_stack())

    my_queue.push(10)
    my_queue.push(15)
    my_queue.push(20)
    print(my_queue.pop())
    print(my_queue.get_queue())


if __name__ == "__main__":
    main()
