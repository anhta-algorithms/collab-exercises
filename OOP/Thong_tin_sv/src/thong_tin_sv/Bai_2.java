/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thong_tin_sv;

import java.util.Scanner;

/**
 *
 * @author tuan
 */
public class Bai_2{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner imput = new Scanner(System.in);
        SV[] danh_sach_SV = new SV[105];
        int so_luong_SV = Integer.parseInt(imput.nextLine());
        int so_luong_Lop = 0;
        Lop[] danh_sach_lop = new Lop[105];
        
        System.out.println("Nhap thong tin sih vien: ");
        for(int i = 1;i <= so_luong_SV;i++)
        {
            System.out.println("Nhap sinh vien " + i + ": ");
            danh_sach_SV[i] = new SV(imput);
        }
        
        System.out.println("Xuat thong tin sinh vien theo ten: ");
        for(int i = 1;i <= so_luong_SV;i++)
        {
            System.out.println(danh_sach_SV[i].get_ThongTin());
        }
        
        System.out.println("Sap xep theo ten: ");
        for(int i = 1;i < so_luong_SV;i++)
        {
            for(int j = i+1;j <= so_luong_SV;j++)
            {
                if(danh_sach_SV[i].get_Ten().compareTo(danh_sach_SV[j].get_Ten()) > 0)
                {
                    SV tmp = danh_sach_SV[i];
                    danh_sach_SV[i] = danh_sach_SV[j];
                    danh_sach_SV[j] = tmp;
                }
            }
        }
        for(int i = 1;i <= so_luong_SV;i++)
        {
            System.out.println(danh_sach_SV[i].get_ThongTin());
        }
        
        System.out.println("In danh sach sinh vien theo theo lop: ");
        for(int i = 1;i <= so_luong_SV;i++)
        {
            int kt = 0;
            for(int j = 1;j <= so_luong_Lop;j++)
            {
                if(danh_sach_SV[i].get_ten_lopSV().compareTo(danh_sach_lop[j].get_ten_lop()) == 0)
                {
                    danh_sach_lop[j].add_Student(danh_sach_SV[i]);
                    kt = 1;
                    break;
                }
            }
            if(kt == 0)
            {
                so_luong_Lop++;
                danh_sach_lop[so_luong_Lop] = new Lop();
                danh_sach_lop[so_luong_Lop].set_ten_lop(danh_sach_SV[i].get_Ten());
                danh_sach_lop[so_luong_Lop].add_Student(danh_sach_SV[i]);
            }
        }
        
        for(int i = 1;i <= so_luong_Lop;i++)
        {
            danh_sach_lop[i].Xuat_thong_tin_lop();
        }
    }
}