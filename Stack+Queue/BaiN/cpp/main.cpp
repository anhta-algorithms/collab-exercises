#include <iostream>
using namespace std;
int rs = 100;
string s;
int NGOAC_MO, NGOAC_DONG;
void Solve()
{
	rs = 0;
	cin >> s;
	int len = s.length(), i = 0;
	NGOAC_MO = 0;NGOAC_DONG = 0;
	
	for(;i < len;i++)
	{
		if(s[i] == '(')
		{
			NGOAC_MO++;
		}
		else if(s[i] == ')' && NGOAC_MO != 0)
		{
			NGOAC_MO--;
		}
		else
		{
			NGOAC_DONG++;
		}
	}

	if(NGOAC_MO != 0 && NGOAC_DONG != 0)
	{
		cout << NGOAC_MO/2 + NGOAC_DONG/2 + NGOAC_DONG%2 + NGOAC_MO%2 << endl;
	}
	else if(NGOAC_MO != 0)
	{
		cout << NGOAC_MO/2 << endl;
	}
	else
	{
		cout << NGOAC_DONG/2 << endl;
	}
}
int main()
{
	int t = 1;
	cin >> t;
	while(t--)
	{
		Solve();
	}
	return 0;
}
