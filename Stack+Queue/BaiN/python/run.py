# coding: utf8

import re
import math
from utils import Starter


class MyStarter(Starter):

    """Docstring for MyStarter. """

    def __init__(self, **kwargs):
        """TODO: to be defined1. """
        Starter.__init__(self, **kwargs)

    def validate_test_sequence(self, sequence):
        if len(sequence) % 2 != 0:
            return False

        expectedPattern = r"^[(|)]+$"
        result = re.match(expectedPattern, sequence)

        if result is None:
            return False
        return True


class Repairer:

    """To count the number of parentheseses that need to be fixed so
    that the new sequence is a correct sequence"""

    def __init__(self, tests_list):
        """TODO: to be defined1. """
        self.tests_list = tests_list

    def repair(self):
        for sequence in self.tests_list:
            print(self.count_fixes(sequence))

    def count_fixes(self, sequence):
        open_parentheses = 0
        close_parentheses = 0
        num_of_fixes = 0

        for i in range(0, len(sequence)):
            if sequence[i] == '(':
                open_parentheses += 1
            elif sequence[i] == ')' and open_parentheses > 0:
                open_parentheses -= 1
            else:
                close_parentheses += 1

        num_of_fixes = math.floor(open_parentheses/2) +\
                open_parentheses % 2 +\
                math.floor(close_parentheses/2) +\
                close_parentheses % 2

        return num_of_fixes


if __name__ == "__main__":
    my_starter = MyStarter()
    my_starter.run()

    my_repairer = Repairer(my_starter.tests_list)
    my_repairer.repair()
