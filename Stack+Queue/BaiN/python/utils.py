# coding: utf8
from abc import ABC, abstractmethod


class Starter(ABC):
    def __init__(self):
        self.num_of_tests = 0
        self.tests_list = []

    def read_num_of_tests(self):
        self.num_of_tests = int(input("Enter number of tests: "))

    def input_each_test(self, test_idx):
        my_input = input("Enter the sequence of test {}: "
                         .format(test_idx))
        while(not(self.validate_test_sequence(my_input))):
            print("Invalid sequence, please try again.")
            my_input = input("Enter the sequence of test {}: "
                             .format(test_idx))
        return my_input

    @abstractmethod
    def validate_test_sequence(self, sequence):
        """TODO: Docstring for validate_test_sequence.
        abstract method to validate the sequence of a test
        if necessary

        Args:
            test_length (TODO): TODO
            sequence (TODO): TODO

        Returns: TODO

        """
        pass

    def run(self):
        self.read_num_of_tests()
        test_idx = 0
        while(test_idx < self.num_of_tests):
            self.tests_list.append(
                self.input_each_test(test_idx))
            test_idx += 1


class SimpleStarter(Starter):
    """
    Starter without any validator for the sequence
    """
    def validate_test_sequence(self, sequence):
        return True


def test():
    starter = Starter()
    starter.run()

    my_starter = SimpleStarter()
    my_starter.run()


if __name__ == "__main__":
    test()
