#include <iostream>
#include<stdio.h> 
#include<stdlib.h>
using namespace std;
struct Node
{
	int data = 0;
	struct  Node *next = NULL;
	struct Node *previous = NULL;
};
struct Node* Head = NULL;
struct Node* Tail = NULL;
int size = 0;
void Push_front(int data)
{
	size++;
	if(Head == NULL)
	{
		Head = new Node;
		Head->data = data;
		Head->next = Tail;
		Head->previous = NULL;
		
		Tail = Head;
	}
	else
	{
		Node *A = Head;
		
		Head = new Node;
		Head->data = data;
		Head->previous = NULL;
		Head->next = A;
		
		A->previous = Head;
	}
}
void Push_back(int data)
{
	size++;
	if(Tail == NULL)
	{
		Tail = new Node;
		Tail->data = data;
		Tail->next = NULL;
		Tail->previous = Head;
		
		Head = Tail;
	}
	else
	{
		Node *B = Tail;
		
		Tail = new Node;
		Tail->data = data;
		Tail->next = NULL;
		Tail->previous = B;
		
		B->next = Tail;
		
	}
}
void Pop_front()
{
	size--;
	if(Head != NULL && Head->next != NULL)
	{
		Node *A = Head->next;
		delete Head;
		Head = A;
		Head->previous = NULL;
	}
	else if(Head != NULL)
	{
		delete Head;
		Head = NULL;
		Tail = NULL;
	}
}
void Pop_back()
{
	size--;
	if(Tail != NULL && Tail->previous != NULL)
	{
		Node *B = Tail->previous;
		delete Tail;
		Tail = B;
		Tail->next = NULL;
	}
	else if(Tail != NULL)
	{
		delete Tail;
		Head = NULL;
		Tail = NULL;
	}
}
void Get_front()
{
	if(Head == NULL)
	{
		cout << "NONE" << endl;
	}
	else 
	{
		cout << Head->data << endl;
	}
}
void Get_back()
{
	if(Tail == NULL)
	{
		cout << "NONE" << endl;
	}
	else 
	{
		cout << Tail->data << endl;
	}
}
void Solve()
{
	string s;
	int k;
	int t;
	cin >> t;
	while(t--)
	{
		cin >> s;
		if(s == "PUSHFRONT")
		{
			cin >> k;
			Push_front(k);
		}
		else if(s == "PUSHBACK")
		{
			cin >> k;
			Push_back(k);
		}
		else if(s == "POPFRONT")
		{
			Pop_front();
		}
		else if(s == "POPBACK")
		{
			Pop_back();
		}
		else if(s == "PRINTFRONT")
		{
			Get_front();
		}
		else if(s == "PRINTBACK")
		{
			Get_back();
		}
	}
}
int main()
{
	//Solve_demo();
	Solve();
	return 0;
}
