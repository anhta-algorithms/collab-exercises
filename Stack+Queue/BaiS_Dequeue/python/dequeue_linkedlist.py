class LinkedList():

    """Docstring for LinkedList. """

    def __init__(self):
        """TODO: to be defined1. """
        self.head = None
        self.tail = None
        self.size = 0

class Node():

    """Docstring for Node. """

    def __init__(self, data):
        """TODO: to be defined1. """
        self.data = data
        self.next = None
        self.previous = None

class Dequeue():

    """Docstring for dequeue. """

    def __init__(self):
        """TODO: to be defined1. """
        self.linkedlist = LinkedList()

    def push_front(self, data):
        """TODO: Docstring for push_front.

        Args:
            data (TODO): TODO

        Returns: TODO

        """
        new_head = Node(data)
        if self.linkedlist.head is None:
            self.linkedlist.head = new_head
            self.linkedlist.tail = new_head
        else:
            old_head = self.linkedlist.head
            old_head.previous = new_head
            new_head.next = old_head
            self.linkedlist.head = new_head
        self.linkedlist.size += 1

    def push_back(self, data):
        """TODO: Docstring for push_back.

        Args:
            arg1 (TODO): TODO

        Returns: TODO

        """
        new_tail = Node(data)
        if self.linkedlist.tail is None:
            self.linkedlist.head = new_tail
            self.linkedlist.tail = new_tail
        else:
            old_tail = self.linkedlist.tail
            old_tail.next = new_tail
            new_tail.previous = old_tail
            self.linkedlist.tail = new_tail
        self.linkedlist.size += 1

    def pop_front(self):
        """TODO: Docstring for pop_front.
        Returns: TODO

        """
        try:
            old_head = self.linkedlist.head
            new_head = self.linkedlist.head.next
            self.linkedlist.head = new_head
            del old_head
        except AttributeError as e:
            self.linkedlist.head = None
        except Exception as e:
            raise e

        """
        Head and tail are referenced to the same address. But in python,
        we cannot delete the referenced variable via its references, so
        we will need to manually empty head and tail instead of just emptying
        the referenced variable
        """
        if self.linkedlist.size == 1:
            self.linkedlist.tail = None
        self.linkedlist.size -= 1

    def pop_back(self):
        """TODO: Docstring for pop_back.
        Returns: TODO

        """
        try:
            old_tail = self.linkedlist.tail
            new_tail = current_tail.previous
            self.linkedlist.tail = new_tail
            del old_tail
        except AttributeError as e:
            self.linkedlist.tail = None
        except Exception as e:
            raise e

        """
        Head and tail are referenced to the same address. But in python,
        we cannot delete the referenced variable via its references, so
        we will need to manually empty head and tail instead of just emptying
        the referenced variable
        """
        if self.linkedlist.size == 1:
            self.linkedlist.head = None
        self.linkedlist.size -= 1

    def peek_front(self):
        """TODO: Docstring for peek_front.
        Returns: TODO

        """
        try:
            print self.linkedlist.head.data
        except AttributeError as e:
            print None

    def peek_back(self):
        """TODO: Docstring for peek_back.
        Returns: TODO

        """
        try:
            print self.linkedlist.tail.data
        except AttributeError as e:
            print None

    def size(self):
        """TODO: Docstring for size.
        Returns: TODO

        """
        return self.linkedlist.size

    def make_empty(self):
        """TODO: Docstring for make_empty.
        Returns: TODO

        """
        node = self.linkedlist.head
        while node is not None:
            next_node = node.next
            del node
            node = next_node
        self.linkedlist.head = None
        self.linkedlist.tail = None


if __name__ == "__main__":
    dequeue = Dequeue()
    dequeue.push_back(1)
    dequeue.push_front(2)
    dequeue.push_back(3)
    dequeue.peek_front()
    dequeue.pop_front()
    dequeue.peek_front()
    dequeue.pop_front()
    dequeue.peek_back()
    dequeue.pop_front()
    dequeue.peek_back()
