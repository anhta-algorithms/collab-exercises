#include <bits/stdc++.h>
using namespace std;
struct Mang
{
	int x[7];	
};
Mang a, b;
int rs = 0;
void Init()
{
	for(int i = 1;i <= 6;i++)
	{
		cin >> a.x[i];
	}
	for(int i = 1;i <= 6;i++)
	{
		cin >> b.x[i];
	}
}
bool Check(Mang a,Mang b)
{
	for(int i = 1;i <= 6;i++)
	{
		if(a.x[i] == b.x[i]) return false;
	}
	return true;
}
void Xoay_trai(Mang &a,Mang b)
{
	a.x[1] = b.x[4];
	a.x[2] = b.x[1];
	a.x[3] = b.x[3];
	a.x[4] = b.x[5];
	a.x[5] = b.x[2];
	a.x[6] = b.x[6];
}
void Xoay_phai(Mang &a,Mang b)
{
	a.x[1] = b.x[1];
	a.x[2] = b.x[4];
	a.x[3] = b.x[2];
	a.x[4] = b.x[4];
	a.x[5] = b.x[6];
	a.x[6] = b.x[3];
}
/*
void Show(Mang d)
{
	for(int i = 1;i <= 6;i++)
	{
		cout << d.x[i] << " ";
	}
	cout << endl;
}
*/
void Solve()
{
	rs = 0;
	queue<pair<Mang,int> > q;
	q.push(make_pair(a,0));
	while(true)
	{
		Mang d;
		rs = q.front().second+1;
		Xoay_trai(d,q.front().first);
		q.push(make_pair(d,q.front().second+1));
		if(Check(d,b)) break;
		Xoay_phai(d,q.front().first);
		if(Check(d,b)) break;
		q.push(make_pair(d,q.front().second+1));
		q.pop();
	}
	cout << rs << endl;
}
int main()
{
	Init();
	Solve();
	return 0;
}
