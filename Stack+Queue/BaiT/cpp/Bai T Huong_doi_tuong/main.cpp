#include <bits/stdc++.h>
using namespace std;
class Mang
{
	private:
		int x1, x2, x3, x4, x5, x6;
	public:
		Mang()
		{
			this->x1 = 0;
			this->x2 = 0;
			this->x3 = 0;
			this->x4 = 0;
			this->x5 = 0;
			this->x6 = 0;
		}
		void Nhap()
		{
			cin >> x1;
			cin >> x2;
			cin >> x3;
			cin >> x4;
			cin >> x5;
			cin >> x6;
		}
		void Gan(int a[])
		{
			this->x1 = a[1];
			this->x2 = a[2];
			this->x3 = a[3];
			this->x4 = a[4];
			this->x5 = a[5];
			this->x6 = a[6];
		}
		void Xuat(int a[])
		{
			a[1] = this->x1;
			a[2] = this->x2;
			a[3] = this->x3;
			a[4] = this->x4;
			a[5] = this->x5;
			a[6] = this->x6;
		}
		void Xoay_trai(int a[])
		{
			a[1] = this->x4;
			a[2] = this->x1;
			a[3] = this->x3;
			a[4] = this->x5;
			a[5] = this->x2;
			a[6] = this->x6;
		}
		void Xoay_phai(int a[])
		{
			a[1] = this->x1;
			a[2] = this->x5;
			a[3] = this->x2;
			a[4] = this->x4;
			a[5] = this->x6;
			a[6] = this->x3;
		}
		void Show()
		{
			cout << this->x1 << " " << this->x2 << " " << this->x3 << " " << this->x4 << " " << this->x5 << " " << this->x6 << endl;
		}
};
int rs = 0;
Mang a,b;
void Init()
{
	a.Nhap();
	b.Nhap();
}
bool Check(Mang a,Mang b)
{
	int N[7], M[7];
	a.Xuat(N);
	b.Xuat(M);
	for(int i = 1;i <= 6;i++)
	{
		if(N[i] != M[i]) 
		{
			return false;
		}
	}
	return true;
}
void Solve()
{
	queue<pair<Mang,int> > q;
	q.push(make_pair(a,0));
	rs = 0;
	while(true)
	{
		rs++;
		int M[7];
		Mang d = q.front().first;
		d.Xuat(M);
		Mang d1, d2;
		d.Xoay_phai(M);
		d1.Gan(M);
		q.push(make_pair(d1,q.front().second+1));
		if(Check(d1,b))
		{
			rs = q.front().second+1;
			break;
		}
		d.Xoay_trai(M);
		d2.Gan(M);
		q.push(make_pair(d2,q.front().second));
		if(Check(d2,b))
		{
			rs = q.front().second+1;
			break;
		}
		q.pop();
	}
	cout << rs << endl;
}
int main()
{
	Init();
	Solve();
	return 0;
}
