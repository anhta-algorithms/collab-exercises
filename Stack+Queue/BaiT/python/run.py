# coding: utf8

import math
import re
from collections import deque
from utils import Starter


class MyStarter(Starter):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def validate_test_sequence(self, sequence):
        expectedPattern = r"^([1-6]\s)*[1-6]$"
        if re.match(expectedPattern, sequence):
            return True
        return False


class Solver:

    """Docstring for Solver. """

    def __init__(self, original_state, target_state):
        """TODO: to be defined1. """
        self.original_state = original_state
        self.target_state = target_state
        self.states_queue = deque([original_state])

    @staticmethod
    def _rotate_leff(current_state):
        new_state = current_state[:]
        new_state[0] = current_state[3]
        new_state[1] = current_state[0]
        new_state[4] = current_state[1]
        new_state[3] = current_state[4]
        return new_state

    @staticmethod
    def _rotate_right(current_state):
        new_state = current_state[:]
        new_state[1] = current_state[4]
        new_state[2] = current_state[1]
        new_state[5] = current_state[2]
        new_state[4] = current_state[5]
        return new_state

    def find_min_moves(self):
        moves = 0
        current_state = self.states_queue.popleft()
        while (current_state != self.target_state):
            self.states_queue.append(Solver._rotate_leff(current_state))
            self.states_queue.append(Solver._rotate_right(current_state))
            current_state = self.states_queue.popleft()
            moves += 1

        return math.floor(math.log(moves, 2))


if __name__ == "__main__":
    my_starter = MyStarter()
    my_starter.run()

    original_state = my_starter.tests_list[0].split(" ")
    original_state = [int(str_num) for str_num in original_state]

    target_state = my_starter.tests_list[1].split(" ")
    target_state = [int(str_num) for str_num in target_state]

    my_solver = Solver(original_state, target_state)
    print(my_solver.find_min_moves())
