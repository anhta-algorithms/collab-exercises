#include <iostream>
#include <queue>
#include <string.h>
using namespace std;
queue <pair<int, int> > q1, q2;
queue <pair<int, int> > q;
void Process_string(string s,queue <pair<int,int> > &q)
{
	int len = s.length();
	for(int i = 1;i < len-3;i++)
	{
		if(s[i] == '*' && s[i+1] == 'x' && s[i+2] == '^')
		{
			int index_num = 0, coe = 0;
			int p1 = i-1, p2 = i+3, k = 1;
			while(s[p1] != ' ')
			{
				coe += (s[p1]-'0')*k;
				k = k*10;
				if(p1 == 0) break;
				p1--;
			}
			while(s[p2] != ' ')
			{
				index_num = index_num*10 + (s[p2]-'0');
				if(p2 == len-1) break;
				p2++;
			}
			q.push(make_pair(index_num,coe));
		}
	}
	
}
void Init()
{
	string s;
	getline(cin,s);
	Process_string(s,q1);
	getline(cin,s);
	Process_string(s,q2);
}
void Solve()
{
	while(!q1.empty() || !q2.empty())
	{
		if(q1.front().first > q2.front().first)
		{
			q.push(make_pair(q1.front().first ,q1.front().second));
			q1.pop();
		}
		else if(q1.front().first < q2.front().first)
		{
			q.push(make_pair(q2.front().first,q2.front().second));
			q2.pop();
		}
		else 
		{
			q.push(make_pair(q1.front().first,q1.front().second + q2.front().second));
			q1.pop();
			q2.pop();
		}
		
	}
	
	while(!q1.empty())
	{
		q.push(make_pair(q1.front().first,q1.front().second));
		q1.pop();
	}
	
	while(!q2.empty())
	{
		q.push(make_pair(q2.front().first,q2.front().second));
		q2.pop();
	}
}
void Print()
{
	int x[25], y[25];
	int dem = 0;
	while(true)
	{
		dem++;
		x[dem] = q.front().second;
		y[dem] = q.front().first;
		q.pop();
		if(q.empty()) break;
	}
	
	for(int i = 1;i < dem;i++)
	{
		cout << x[i] << "*x^" << y[i] << " + ";
	}
	cout << x[dem] << "*x^" << y[dem] << endl;
}
int main()
{
	int t;
	cin >> t;
	cin.ignore();
	while(t--)
	{
		Init();
		Solve();
		Print();
	}
	return 0;
}
