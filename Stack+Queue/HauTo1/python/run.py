# coding: utf8

from utils import Starter
from data_structures import Stack, Queue


class MyStarter(Starter):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def validate_test_sequence(self, sequence):
        return True


class Converter:
    """
    - To convert from infix to postfix
    - Algorithm can be read from
    https://123doc.org/document/3200539-co-so-ly-thuyet-balan-bieu-thuc-tien-to-trung-to-hau-to-trinh-bay-thuat-toan-va-cai-dat-tren-c.htm
    """
    operations = [
        {
            "char": '+',
            "priority_level": 0
        },
        {
            "char": '-',
            "priority_level": 0
        },
        {
            "char": '*',
            "priority_level": 1
        },
        {
            "char": '/',
            "priority_level": 1
        },
        {
            "char": '^',
            "priority_level": 2
        }
    ]

    def __init__(self, tests_list):
        """TODO: to be defined1.

        Args:
            tests_list (TODO): TODO


        """
        self._tests_list = tests_list

    def convert_all_tests(self):
        for test in self._tests_list:
            print(Converter._convert_from_infix_to_postfix(test))

    @staticmethod
    def _convert_from_infix_to_postfix(test):
        converted_postfix = Queue([])
        stack_of_chars = Stack([])

        for char in test:
            if Converter._is_a_special_char(char):
                stack_of_chars.push(char)
                while len(stack_of_chars.get_stack()) > 1:
                    first_char_in_stack = stack_of_chars.get_stack()[0]
                    second_char_in_stack = stack_of_chars.get_stack()[1]
                    if Converter._get_priority_level(first_char_in_stack) <=\
                       Converter._get_priority_level(second_char_in_stack):
                        first_char_in_stack = stack_of_chars.pop()
                        converted_postfix.push(stack_of_chars.pop())
                        stack_of_chars.push(first_char_in_stack)
                    else:
                        break
            elif char == '(':
                stack_of_chars.push(char)
            elif char == ')':
                poped_char = stack_of_chars.pop()
                while poped_char != '(':
                    converted_postfix.push(poped_char)
                    poped_char = stack_of_chars.pop()
            else:
                converted_postfix.push(char)
            #  print("Current char: " + char)
            #  print("Stack of char: " + str(stack_of_chars.get_stack()))
            #  print("Converted postfix" + str(converted_postfix.get_queue()))
            #  print("\n")

        while len(stack_of_chars.get_stack()) > 0:
            converted_postfix.push(stack_of_chars.pop())
        return Converter._print_queue_as_string(converted_postfix.get_queue())

    @staticmethod
    def _is_a_special_char(char):
        for operation in Converter.operations:
            if char == operation["char"]:
                return True
        return False

    @staticmethod
    def _get_priority_level(operation_char):
        for operation in Converter.operations:
            if operation_char == operation["char"]:
                return operation["priority_level"]
        return -1

    @staticmethod
    def _print_queue_as_string(queue):
        str = ""
        for char in queue:
            str = str + char
        return str


def main():
    my_starter = MyStarter()
    my_starter.run()

    my_converter = Converter(my_starter.tests_list)
    my_converter.convert_all_tests()


if __name__ == "__main__":
    main()
