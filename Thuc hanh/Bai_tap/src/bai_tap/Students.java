/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bai_tap;
import java.util.Scanner;
/**
 *
 * @author tuan
 */
public class Students {
    int ma;
    String name;
    String class_of_student;
    float diem_tb;
    
    Students()
    {
        ma = 0;
        name = "";
        class_of_student = "";
        diem_tb = 0;
    }
    Students(int ma,String name,String class_of_student,float diem_tb)
    {
        this.ma = ma;
        this.name = name;
        this.class_of_student = class_of_student;
        this.diem_tb = diem_tb;
    }
    
    public void set_Ma(int ma)
    {
        this.ma = ma;
    }
    public String get_Ma()
    {
        if(ma < 10) return "000" + ma;
        else if(ma < 100) return "00" + ma;
        else if(ma < 1000) return "0" + ma;
        else return "" + ma;
    }
    
    private String check_Name(Scanner imput)
    {
        String ten = "";
        boolean kt = true;
        while(kt)
        {
            try
            {
                System.out.print("Nhap ten sinh vien " + ": ");
                ten = imput.nextLine();
                char c = ten.charAt(0);
                kt = false;
            }
            catch(StringIndexOutOfBoundsException error)
            {
                System.out.println("Ban quen chua nhap gia tri!");
            }
        }
        return ten;
    }
    public void set_Name(Scanner imput)
    {
        this.name = check_Name(imput);
    }
    public String get_Name()
    {
        return name;
    }
    
    private String check_Class_of_student(Scanner imput)
    {
        String class_of_studentX = "";
        while(true)
        {
            System.out.print("Nhap lop sinh vien " + ": ");
            class_of_studentX = imput.nextLine();
            if((class_of_studentX.charAt(0) == 'B' && class_of_studentX.charAt(1) == '1' && class_of_studentX.charAt(2) == '6' && class_of_studentX.charAt(3) == 'D' && class_of_studentX.charAt(4) == 'C' && class_of_studentX.charAt(5) == 'C' && class_of_studentX.charAt(6) == 'N'))
            {
                String name_of_class = "";
                for(int i = 7;i < class_of_studentX.length();i++) name_of_class += class_of_studentX.charAt(i);
               int n = Integer.parseInt(name_of_class);
               if(n >= 0 && n <= 10) break;
            }
            System.out.println("Ten lop hoc khong dung quy dinh hoac khong ton tai!");
        }
        return class_of_studentX;
    }
    public void set_Class_of_student(Scanner imput)
    {
        this.class_of_student = check_Class_of_student(imput);
    }
    public String get_Class_of_student()
    {
        return class_of_student;
    }
    
    private float check_Diem_tb(Scanner imput)
    {
        float diem_tbX = -1;
        while(true)
        {
            System.out.print("Nhap diem trung binh sinh vien " + ": ");
            diem_tbX = Float.parseFloat(imput.nextLine());
            if(diem_tbX >= 0 && diem_tbX <= 10) break;
            System.out.println("Diem trung binh khong nam trong gioi han!");
        }
        return diem_tbX;
    }
    public void set_Diem_tb(Scanner imput)
    {
        this.diem_tb = check_Diem_tb(imput);
    }
    public float get_Diem_tb()
    {
        return diem_tb;
    }
    
    public static void input_Infor(Students A,Scanner imput,int ma)
    {
        A.set_Ma(ma);
        System.out.println("Nhap thong tin sinh vien co ma " + A.get_Ma() + ": ");
        A.set_Name(imput);
        A.set_Class_of_student(imput);
        A.set_Diem_tb(imput);
    }
    public static void print_Infor(Students A)
    {
        System.out.println("      +      ");
        System.out.println("In thong tin sinh vien co ma " + A.get_Ma() + ": ");
        System.out.println("Ten sinh vien: " + A.get_Name());
        System.out.println("Lop sinh vien: " + A.get_Class_of_student());
        System.out.println("Diem trung binh sinh vien: " + A.get_Diem_tb());
    }
}
    
